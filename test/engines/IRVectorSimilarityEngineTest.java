package engines;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import core.DTMatrix;
import tokeniser.SimpleTokenizer;

/**
 * Some basic tests of calculated similarities for vector similarity engine
 * 
 * Note: documents and queries adjusted to be invariant under stemming
 * @author bjef8061
 *
 */
public class IRVectorSimilarityEngineTest {

    private DTMatrix matrix;
    private Engine engine;

	// Comment this out for debugging
    @Rule
    public Timeout globalTimeout = new Timeout(1000);
    
    @Before
    public void setUp() throws Exception {
        matrix = new DTMatrix(new SimpleTokenizer());
        matrix.addDocument("t1", "Early to bed and early to rise makes a man healthy, wealthy and wise");
        matrix.addDocument("t2", "Always take bananas to a party, bananas are good!");
        matrix.addDocument("t3", "A doctors office provides healthy advice and bananas to patients");
        matrix.addDocument("t4", "A fool thinks himself to be wise, but a wise man knows himself to be a fool.");
    }

    @Test
    public void testExecuteQ1() throws Exception {
        engine = new IRVectorSimilarityEngine(matrix);
        final String query = "healthy wise";
		Map<String, Double> result = engine.execute(query);
        assertEquals("Returning too many elements, only return those with result > 0", 3, result.size());
        checkMatch(query, "t1", result, 0.181238117);
        checkMatch(query, "t3", result, 0.090619058);
        checkMatch(query, "t4", result, 0.181238117);
        checkNotIncluded(result,"t2");
    }

    @Test
    public void testExecuteQ2() throws Exception {
        engine = new IRVectorSimilarityEngine(matrix);
        final String query = "bananas healthy";        
        Map<String, Double> result = engine.execute(query);
        assertEquals("Returning too many elements, only return those with result > 0", 3, result.size());
		checkMatch(query, "t1", result, 0.090619058);
        checkMatch(query, "t2", result, 0.181238117);
        checkMatch(query, "t3", result, 0.181238117);
        checkNotIncluded(result,"t4");
    }
    
	/**
	 * @param result
	 * @param doc
	 */
	private void checkNotIncluded(Map<String, Double> result, final String doc) {
		assertNull("Document " + doc + "should not be included in results since similarity below threshold", result.get(doc));
	}

    private static void checkMatch(String query, String docId, Map<String, Double> results, double expected) {
    	assertEquals("Similarity of \"" + query + "\" to " + docId, expected, results.get(docId), 0.001);
	}

}