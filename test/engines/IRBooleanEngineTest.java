package engines;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import core.DTMatrix;
import tokeniser.SimpleTokenizer;

public class IRBooleanEngineTest {

    private DTMatrix matrix;
    private Engine engine;

	// Comment this out for debugging
    @Rule
    public Timeout globalTimeout = new Timeout(1000);
    
    @Before
    public void setUp() throws Exception {
        matrix = new DTMatrix(new SimpleTokenizer());
        matrix.addDocument("t1", "what a fool, he thinks himself to be wise");
        matrix.addDocument("t2", "a wise man knows himself to be a fool");
        matrix.addDocument("t3", "a truly wise man knows he knows nothing");
        matrix.addDocument("t4", "a wise man does first, what a fool does last");
    }

    @Test
    public void testExecute() throws Exception {
        engine = new IRBooleanEngine(matrix);
        Map<String, Double> result = engine.execute("is a man a fool?");
        assertEquals("Returning too many elements, only return those with result > 0", 2, result.size());
        assertNull("Document t1 should not match", result.get("t1"));
        assertEquals("Document t2 should match exactly", 1.0, (double)result.get("t2"), 0.001);
        assertNull("Document t3 should not match", result.get("t3"));
        assertEquals("Document t4 should match exactly", 1.0, (double)result.get("t4"), 0.001);
    }

    @Test
    public void testCalculateStatistics() throws Exception {
        engine = new IRBooleanEngine(matrix);
        Map<String, Double > result = new HashMap<String, Double>();
        result.put("t1", 0.5);
        result.put("t2", 0.5);
        List<String> expected = Arrays.asList("t1", "t3");
        assertEquals("Recall Algorithm is incorrect", 0.5, engine.calculateRecall(expected, result), 0.001);
        assertEquals("Precision Algorithm is incorrect", 0.5, engine.calculatePrecision(expected, result), 0.001);
        assertEquals("Fallout Algorithm is incorrect", 0.5, engine.calculateFallout(expected, result), 0.001);
    }

    @Test
    public void testCalculateStatisticsComplex() throws Exception {
        // Add more elements
        matrix.addDocument("t5", "a wise man does first, what a fool does last");
        matrix.addDocument("t6", "a wise man does first, what a fool does last");
        matrix.addDocument("t7", "a wise man does first, what a fool does last");
        engine = new IRBooleanEngine(matrix);

        Map<String, Double > result = new HashMap<String, Double>();
        result.put("t1", 0.5);
        result.put("t2", 0.5);
        result.put("t5", 0.2);
        result.put("t6", 0.1);
        result.put("t7", 0.1);

        List<String> expected = Arrays.asList("t1", "t3", "t6");
        assertEquals("Recall Algorithm is incorrect", 0.666, engine.calculateRecall(expected, result), 0.001);
        assertEquals("Precision Algorithm is incorrect", 0.4, engine.calculatePrecision(expected, result), 0.001);
        assertEquals("Fallout Algorithm is incorrect", 0.75, engine.calculateFallout(expected, result), 0.001);
    }
}