package engines;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import core.DTMatrix;
import tokeniser.StemmerTokenizer;

/**
 * Tests the use of the word stemmer in conjunction with the cosine similarity engine
 */
public class StemmedWordsCosineSimilarityTest {

	// Comment this out for debugging
    @Rule
    public Timeout globalTimeout = new Timeout(1000);
    
    private DTMatrix matrix;
    private Engine engine;

    @Before
    public void setUp() throws Exception {
        matrix = new DTMatrix(new StemmerTokenizer());
        matrix.addDocument("t1", "Early to bed and early to rise makes a man healthy, wealthy and wise.");
        matrix.addDocument("t2", "Always take a banana to a party, bananas are good!");
        matrix.addDocument("t3", "A doctors office provides health advice and bananas to patients");
        matrix.addDocument("t4", "A fool thinks himself to be wise, but a wise man knows himself to be a fool.");
        engine = new IRCosineSimilarityEngine(matrix);
    }

    @Test
    public void testExecuteQ1() throws Exception {
        final String query = "health wise";
        Map<String, Double> result = engine.execute(query);
        assertEquals("Returning too many elements, only return those with result > 0", 3, result.size());
    	checkMatch(query, "t1", result, 0.072547625);
        checkMatch(query, "t3", result, 0.390360029);
        checkMatch(query, "t4", result, 0.178885438);
		checkNotIncluded(result, "t2");
    }

	/**
	 * @param result
	 * @param doc
	 */
	private void checkNotIncluded(Map<String, Double> result, final String doc) {
		assertNull("Document " + doc + "should not be included in results since similarity below threshold", result.get(doc));
	}
    
    @Test
    public void testExecuteQ2() throws Exception {
        final String query = "banana health";
        Map<String, Double> result = engine.execute(query);
        assertEquals("Returning too many elements, only return those with result > 0", 2, result.size());
        checkMatch(query, "t2", result, 0.25819889);
        checkMatch(query, "t3", result, 0.487950036);
        assertNull("Document t1 should not be included in results since similarity below threshold", result.get("t1"));
        assertNull("Document t4 should not be included in results since similarity below threshold", result.get("t4"));
    }
    
	private static void checkMatch(String query, String docId, Map<String, Double> results, double expected) {
		assertEquals("Similarity of \"" + query + "\" to " + docId + " (Make sure you're stemming both the document AND query)", expected, results.get(docId), 0.001);
	}

}



