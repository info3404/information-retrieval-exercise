# Week 5 PASTA Homework - Information Retrieval
This weeks homework revolves around informational retrieval using several variants of the Vector Space model. The lecture slides provide a good outline of this.

You are implementing several components of the IR Engine. This will allow you to input several documents, and then perform a query on these documents to find those that are the most similar.

As usual, this week there are 3 different levels you can choose to do, to get 2, 3 or 4 marks respectively.

## Easy

Implement **boolean text search**, in which documents only match if they contain all the terms of the query (ignoring stop words). You need to implement the following methods in the classes:

* `Engine`: `calculateRecall`, `calculatePrecision`, `calculateFallout`
* `IRBooleanEngine`: `execute`
* `DTMatrix`: `addDocument`
* `SimpleTokenizer`: `parse`

The sample tests for this level are:

* `DTMatrixTest`
* `IRBooleanEngineTest`
* `SimpleTokenizerTest`

As part of this, you will need to ignore any stop words that may be in the documents or queries. The list of stop words to use can be found in the `core.Constants` file


## Medium

Implement **vector similarity search**, in which documents and queries are treated as vectors, and their similarity measured according to the dot product of the document and query vectors. The vector elements are given by the *term frequencies (TFs)*, scaled by the corresponding *inverse document frequency (IDF)*.

You will need to implement [Easy], as well as:

* `DTMatrix`: `getInverseDocumentFrequency`
* `IRVectorSimilarityEngine`: `execute`

The sample tests for this level are:

* `IRVectorSimilarityEngineTest`

## Hard

Implement **cosine similarity search**, in which documents and queries are represented as *normalised* (length 1) vectors to avoid bias from large documents. Additionally, document terms are reduced to a smaller set by using **stemming** provided by `PorterStemmer` class (this is done in the string tokeniser step), reproduced from Martin Porter's [PorterStemmer site](https://tartarus.org/martin/PorterStemmer/).

You will need to implement [Medium], as well as the following additional methods:

* `IRCosineSimilarityEngine`: `execute`, `calculateNormalisationScalar`
* `StemmerTokenizer`: `parse`

The sample tests for this level are:

* `IRCosineSimilarityEngineTest`
* `StemmedWordsCosineSimilarityTest`

Note that a spreadsheet `stemmed_cosine_calculations.xlsx` is included to show how the test similarity scores were calculated. By warned that the output from the stemmer is an implementation detail, and will not exactly match the term strings shown in the spreadsheet.