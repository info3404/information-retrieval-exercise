package demo;
import java.util.Arrays;
import java.util.Map;

import core.DTMatrix;
import engines.Engine;
import engines.IRVectorSimilarityEngine;
import tokeniser.SimpleTokenizer;

/**
 * Example usage of the engine
 */
public class IRDemo {

    public static void main(String[] args) {
        DTMatrix matrix = new DTMatrix(new SimpleTokenizer());
        matrix.addDocument("t1", Text.HAMLET);
        matrix.addDocument("t2", Text.EINSTEIN);
        matrix.addDocument("t3", Text.JFK);
        matrix.addDocument("t4", Text.MLK);

        Engine engine = new IRVectorSimilarityEngine(matrix);
        Map<String, Double> results = engine.execute("sleep at night");
        System.out.println(Arrays.toString(results.entrySet().toArray()));
    }
}
