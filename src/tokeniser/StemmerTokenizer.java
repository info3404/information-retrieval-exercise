package tokeniser;

import core.Constants;
import stemmer.PorterStemmer;

import java.util.ArrayList;

/**
 * Variant of SimpleTokenizer that stems words
 */
public class StemmerTokenizer implements Tokenizer {
    @Override
    public String[] parse(String text) {
        /* Workflow:
			1. make a new list and initialize a new porter stemmer
			2. For each word in text
				a. make sure all are characters (replace numbers etc.)
				b. make it lower case for comparison
				c. if it isn't a stop word
					i. check that it isn't empty
					ii. reset your stemmer
					iii. add the stemmed word (stem before adding!)
		*/
        return null;
    }
}
