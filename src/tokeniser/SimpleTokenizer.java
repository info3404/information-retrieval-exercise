package tokeniser;

import core.Constants;
import stemmer.PorterStemmer;

import java.util.ArrayList;

/**
 * Simple tokeniser to convert a text string into an array of tokens.
 * - Remove all punctuation and numbers
 * - Return only the words that remain (delimited by whitespace)
 * - Ignore stop words
 */
public class SimpleTokenizer implements Tokenizer {
    @Override
    public String[] parse(String text) {
       /* Workflow:
	   		1. make a new list
			2. Iterate through words in text
			3. for each word
				a. make sure all are characters (replace numbers etc.)
				b. make it lower case for comparison
				c. if it isn't a stop word and it's not empty (or 0 length)
					-> add to result set
					-> DO NOT STEM BEFORE ADDING
		*/
        return null;
    }

    public String[] stemParse(String text){
        /* Workflow:
			1. make a new list and initialize a new porter stemmer
			2. For each word in text
				a. make sure all are characters (replace numbers etc.)
				b. make it lower case for comparison
				c. if it isn't a stop word
					i. check that it isn't empty
					ii. reset your stemmer
					iii. add the stemmed word (stem before adding!)
		*/
        return null;
    }
}
