package engines;

import core.DTMatrix;
import core.Vector;

import java.util.HashMap;
import java.util.Map;

/**
 * Boolean Retrieval Engine
 */
public class IRBooleanEngine extends Engine {

    /**
     * Initialise the IR Boolean Engine
     */
    public IRBooleanEngine(DTMatrix dtMatrix) {
        //this.dtMatrix = dtMatrix;
    }

    /**
     * Returns the results of executing a query using the argument text
     * 1. Create the query vector
     * 2. For each document that has a term frequency>=1 for each of the query's terms, 
     *    include the document as a match (with similarity 1)
     * @param text - the query to execute
     * @return a map of (document) => (score) for all matches (score 1)
     */
    @Override
    public Map<String, Double> execute(String text) {
        Map<String, Double> results = new HashMap<String, Double>();

        /**
         * Hints: Look at dtMatrix.createQueryVector(...)
         *        Look at Vector.multiply
         */

        /**
         * To iterate over all the documents in the matrix:
            Iterator<Vector> rows = dtMatrix.rows();
            for(int i = 0; rows.hasNext(); i++) {
                Vector row = rows.next();
                /** Do something here
            }
         */
        return results;
    }
}
